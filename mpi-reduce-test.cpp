#include <iostream>
#include <vector>
#include <mpi.h>


int main(int argc, char* argv[]) {
    int size, rank;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &size);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);

    const int M = 100000;
    int n = 16;

    if (argc == 2) {
        n = std::stoi(argv[1]);
        if (n < 1) n = 16;
    }

    if (rank == 0) {
        std::cout << "communicator size: " << size << std::endl;
        std::cout << "message size: " << n << " int" << std::endl;
        std::cout << "running " << M << " realizations of MPI_Reduce..." << std::endl;
    }


    char name[MPI_MAX_PROCESSOR_NAME];
    int len;

    MPI_Get_processor_name(name, &len);

    MPI_Status stat;
    int flag = 0;

    if (rank == 0) {
        std::cout << "rank: " << rank << ", host: " << name << std::endl;
        MPI_Send(&flag, 1, MPI_INT, rank + 1, 111, MPI_COMM_WORLD);
    } else if (rank == size - 1) {
        MPI_Recv(&flag, 1, MPI_INT, rank - 1, 111, MPI_COMM_WORLD, &stat);
        std::cout << "rank: " << rank << ", host: " << name << std::endl;
    } else {
        MPI_Recv(&flag, 1, MPI_INT, rank - 1, 111, MPI_COMM_WORLD, &stat);
        std::cout << "rank: " << rank << ", host: " << name << std::endl;
        MPI_Send(&flag, 1, MPI_INT, rank + 1, 111, MPI_COMM_WORLD);
    }


    std::vector<int> ibuf(n);
    std::vector<int> obuf(n);

    auto t0 = MPI_Wtime();

    for (int i = 0; i < M; ++i) {
        MPI_Reduce(obuf.data(), ibuf.data(), n, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
    }

    auto t1 = MPI_Wtime();

    if (rank == 0) std::cout << "total time: " << (t1 - t0) << std::endl;

    return MPI_Finalize();
} // main
