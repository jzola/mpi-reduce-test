Simple demo showing how to build Singularity container with Intel MPI, assuming binding of MPI on the execution host.
Make sure that Intel MPI with support for `libfabric` is available on the host system.
